//
//  Quote.swift
//  Quotes
//
//  Created by Jeyakumaran Mayooresan on 5/23/15.
//  Copyright (c) 2015 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit

class Quote {
   
    var quote : String = "";
    var quoteAuthor : String = "";
    
    init(quote : String, quoteAuthor : String){
        
        self.quote = quote
        self.quoteAuthor = quoteAuthor
    }
    
    
}
