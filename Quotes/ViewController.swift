//
//  ViewController.swift
//  Quotes
//
//  Created by Jeyakumaran Mayooresan on 5/23/15.
//  Copyright (c) 2015 Jeyakumaran Mayooresan. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {
    
    var quotesArray = [Quote]()

    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBAction func newQuoteButtonPressed(sender: AnyObject) {
        
        if quotesArray.count > 0 {
            self.generateNewQuote()
        }else{
            println("No quotes found")
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        SVProgressHUD.showWithStatus("Please wait...", maskType : SVProgressHUDMaskType.Gradient)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Getting data from parse.com
        var query = PFQuery(className: "Quote")
        query.findObjectsInBackgroundWithBlock { (objects : [AnyObject]?, error : NSError?) -> Void in
            
            SVProgressHUD.dismiss()
            
            if error == nil {
                // The find succeeded.
                println("Successfully retrieved \(objects!.count) quotes.")
                // Do something with the found objects
                if let objects = objects as? [PFObject] {
                    for object in objects {
                       
                        
                        var quote : String = object["quote"] as! String
                        var quoteAuthor : String = object["quoteAuthor"]as! String

                        var currentQuote = Quote(quote: quote, quoteAuthor: quoteAuthor)
                        self.quotesArray.append(currentQuote)
                        
                        
                    }
                    
                    self.generateNewQuote()
                }
            }else{
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
    }
    
    func generateNewQuote(){

        var diceValue : Int = Int(arc4random_uniform(UInt32(quotesArray.count)))
        
        println(diceValue)
        
        var selectedQuote = quotesArray[diceValue]
        
        quoteLabel.text = selectedQuote.quote
        authorLabel.text = selectedQuote.quoteAuthor
    }



}

